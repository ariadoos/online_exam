<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

//index routes
Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

//authentication routes
Auth::routes();
Route::get('home', 'HomeController@index')->name('home')->middleware('isadmin');
Route::get('/admin' , 'admin\homeController@index')->name('admin');

//pdf download routes
Route::get('/admin/download' , 'pdfController@index');
Route::get('/admin/print' , 'pdfController@result');


//admin dashboard routes
Route::resource('/admin/students' , 'admin\viewStudentsController');
Route::resource('/admin/questions' , 'admin\manageQuestionsController');
Route::resource('/admin/subject' , 'admin\subjectController');
Route::resource('/admin/time' , 'admin\setTimeController');
Route::resource('/admin/role' ,'admin\roleController');
Route::resource('/admin/user' , 'admin\userController');
Route::get('/admin/publish' , 'admin\homeController@publish');
Route::post('/admin/publish' , 'admin\homeController@store');

//user routes
Route::get('/home/display' , 'HomeController@display');
Route::get('home/result' , 'HomeController@result');

//submit the answer
Route::post('/home/submit' , 'HomeController@store');



