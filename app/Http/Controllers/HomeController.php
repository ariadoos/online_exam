<?php

namespace App\Http\Controllers;


use App\answeruser;
use App\publish;
use App\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Question;
use App\User;
use App\Result;
use Illuminate\Support\Facades\Auth;
use App\Settime;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $id = Auth::user()->id;

        $publish_check = publish::all();

        $mytime = Carbon::now();

        $mytime->setTimezone('Asia/Kathmandu');

        $current_date =strtotime($mytime->toDateString($mytime)); //numeric format ma xa

        $current_time =strtotime($mytime->toTimeString($mytime)) ; //numeric format ma nai xa

        $db_time = settime::all();

        $date;
        $stime ;
        $etime ;

        foreach($db_time as $db)
        {
            $date = strtotime($db->examdate);

            $stime = $db->startingtime;

            $etime = $db->endingtime ;
        }



        $result_row = result::where('user_id' , '=' , $id)->count();

        return view('user.index' , compact('result_row' , 'publish_check' , 'current_date' , 'current_time' , 'date' , 'stime' , 'etime'));
    }






    public function display()
    {
        $id = Auth::user()->id;

        $check = answeruser::where('user_id' , '=', $id)->count();

        $time = settime::all();

        $var = $this->diff($time[0]->startingtime, $time[0]->endingtime);
//        session_destroy(); die;




//        die;
//        if(array_key_exists('lasttime', $_SESSION))
//        {
//            $new_var= floor (time()-$_SESSION['lasttime']);
//            $var = ($var * 60) - $new_var;
//            $var = intval($var / 60);
//        }
//
//        $_SESSION['lasttime']=time();


        if($check != 0)
        {
            $question = answeruser::all();

            return view('user.home' , compact('question' , '$id' , 'check' , 'var'));
        }
        else{
            $question = answeruser::all();

            $time = settime::all();

            $subject = Subject::inRandomOrder()->get();

            return view('user.home' , compact('subject' , 'id' , 'check' , 'question' , 'var'));

        }

    }

    public function store(Request $request)
    {
       $success = answeruser::store($request);

       $check =  answeruser::checkAnswer($request);

       return view('user.thank');

    }

    public function diff($starttime , $endtime)
    {
        $diff = $endtime - $starttime ;

        return ($diff/60) ;

//        return strtotime(date('H:i:s', $starttime)) - strtotime(date('H:i:s', $endtime));
//return 0;
    }


    public function result()
    {
        $uid = Auth::user()->id;

        $user = user::find($uid);

        $results = Result::where('user_id' , '=' , $uid)->get();

        $totalQuestion = answeruser::where('user_id' , '=' , $uid)->count();

        $unsolved =answeruser::where('answer_id' , '=' , NULL)->where('user_id' , '=' , $uid)->count();

        return view('user.result' , compact('results' , 'unsolved' , 'totalQuestion' ,'user'));

    }
}

