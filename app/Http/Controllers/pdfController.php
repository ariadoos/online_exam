<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Randompassword;
use PDF;

class pdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('role:admin');


    }

    public function index()
    {
        $view = \View::make('pdf');

        $html_content = $view->render();

        PDF::SetTitle('Random Password list');

        PDF::AddPage();

        PDF::writeHTML($html_content, true, false, true, false, '');

        PDF::Output('randomPasswordList.pdf');
    }

    public function result()
    {
        $view = \View::make('print');

        $html_content = $view->render();

        PDF::SetTitle('Result list');

        PDF::AddPage();

        PDF::writeHTML($html_content, true, false, true, false, '');

        PDF::Output('resultList.pdf');
    }

}
