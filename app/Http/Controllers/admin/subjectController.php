<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use Session;


class subjectController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('permission:create-subject')->only('create');

        $this->middleware('permission:edit-subject')->only('edit');

        $this->middleware('permission:list-subject')->only('index');

        $this->middleware('permission:delete-subject')->only('delete');
    }

    public function index()
    {
        $subject=Subject::all();

        $row=Subject::all()->count();

        return view('admin.subjects.subject' , compact('subject' , 'row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subjects.addsubject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $subject = Subject::all();

        $row = Subject::all()->count();

        $data = new Subject;

        if ($data->check($request->subName)) {

            $data->subname = strtolower($request->subName);

            $data->weigh = $request->weigh;

            $data->save();

            Session::flash('success', 'Subject Successfully Added');

            return redirect('/admin/subject/create')->with('subject', 'row');
        }

        Session::flash('error', 'Subject name Already Exist');

        return redirect('/admin/subject/create')->with('subject', 'row');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject=Subject::find($id);

        return view('admin.subjects.editsubject' , compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = Subject::all();

        $row = Subject::all()->count();

        $obj = new Subject;

        if($obj->check($request->subName))
        {
            if($obj->updat($request , $id))
            {
                $request->session()->flash('success' , "Updated Successfully");

                return redirect('/admin/subject')->with('data' , 'row');
            }
        }
        $request->session()->flash('success' , "No change");

        return redirect('/admin/subject')->with('data' , 'row');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $subject=Subject::find($id);

        $subject->delete();

        $subject = Subject::all();

        $row = Subject::all()->count();

        Session::flash('error' , "Deleted Successfully");

        return redirect('/admin/subject')->with('data' , 'row');
    }
}
