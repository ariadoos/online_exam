<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Session;
use Illuminate\Support\Facades\DB;

class roleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }


    public function index()
    {
        $roles= Role::all();
        return view('admin.roles.role' , compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions=Permission::all();

        return view('admin.roles.create' , compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role= new Role;

        $role->name=$request->name;

        $role->display_name=$request->display_name;

        $role->description = $request->description;

        $role->save();

        foreach($request->permission as $key=>$value)
        {
            $role->attachPermission($value);
        }

        Session::flash('success', 'Role Created');

        return redirect('/admin/role');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        $permissions =Permission::all();

        $role_permission = $role->perms()->pluck('id' , 'id')->toArray();

        return view('admin.roles.edit' , compact('role' , 'permissions' , 'role_permission'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role= Role::find($id);

        $role->name=$request->name;

        $role->display_name=$request->display_name;

        $role->description = $request->description;

        $role->save();

        DB::table('permission_role')->where('role_id' ,$id)->delete();

        foreach($request->permission as $key=>$value)
        {
            $role->attachPermission($value);
        }

        Session::flash('success', 'Role Updated');

        return redirect('/admin/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=Role::find($id);

        $role->delete();

        Session::flash('error' , "Deleted Successfully");

        return redirect('/admin/role');
    }
}
