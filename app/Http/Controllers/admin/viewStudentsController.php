<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Session;
use Illuminate\Support\Facades\DB;

class viewStudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('permission:create-student')->only('create');

        $this->middleware('permission:edit-student')->only('edit');

        $this->middleware('permission:list-student')->only('index');

        $this->middleware('permission:delete-student')->only('delete');
    }

    public function index()
    {
        $users=User::where('role','=' , 'user')->get();

        return view('admin.students.viewStudents' , compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.students.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ins = new User;

        $ranpass = str_random(10);

        $this->validate($request , [

            'entrance_number'=>'required|integer',
            'first_name'=> 'required',
            'last_name'=>'required',
            'email'=>'required|unique:users|email',
            'address'=>'required',
            'phone_number'=>'required',
            'sex'=>'required',

        ]);

        DB::table('randompasswords')->insert([
            'entrance_id'=> $request->entrance_number,
            'password' => $ranpass,

        ]);

        $ins->entrance_id = $request->entrance_number;

        $ins->firstname = $request->first_name;

        $ins->lastname = $request->last_name;

        $ins->email = $request->email;

        $ins->password = bcrypt($ranpass);

        $ins->address = $request->address;

        $ins->phonenumber = $request->phone_number;

        $ins->sex = $request->sex;

        $test=$ins->save();

        Session::flash('success', 'Successfully Added');

        return redirect('/admin/students');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users=User::find($id);
        return view ('admin.students.editregister' , compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ins = User::find($id);

        $ins->entrance_id = $request->entrance_number;

        $ins->firstname = $request->first_name;

        $ins->lastname = $request->last_name;

        $ins->email = $request->email;

        $ins->address = $request->address;

        $ins->phonenumber = $request->phone_number;

        $ins->sex = $request->sex;

        $test=$ins->save();

        Session::flash('success', 'Updated Successfully ');

        return redirect('/admin/students');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);

        $e_id = $data->entrance_id;

        $data->delete();

        DB::table('randompasswords')->where('entrance_id' , $e_id)->delete();

        Session::flash('error', 'Deleted Succefully');

        return redirect('/admin/students');
    }
}
