<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Question;
use App\Answer;
use Session;

class manageQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('permission:create-question')->only('create');

        $this->middleware('permission:edit-question')->only('edit');

        $this->middleware('permission:list-question')->only('index');

        $this->middleware('permission:delete-question')->only('delete');
    }

    public function index(Request $request)
    {
        $subject = Subject::all();

        $key=$request->subject;

        if(is_numeric($key))
        {
            $subj = Subject::find($key);

            return view('admin.questions.search' , compact('subject' , 'subj'));
        }

        return view('admin.questions.viewQuestions' , compact('subject' , 'key'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Subject::all();

        return view('admin.questions.addQuestion' , compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Question::addQuestion($request);

        $correct_ans = Answer::addAnswer($request , $id );

        foreach($correct_ans as $correct)
        {
            $data = $correct->id;
        }

        $query = DB::table('questions')->where('id' ,'=' , $id)->update(['correct_answer_id' => $data]);

        if($query)
        {
            Session::flash('success', 'Successfully Added');

            return redirect('/admin/questions/create');
        }

        Session::flash('success', 'Error Occured');

        return redirect('/admin/questions/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Question::find($id)->subject;

        $question = Question::find($id);

        $answer = Question::find($id)->answers;

        $data = DB::table('subjects')->whereNotIn('id' , [$subject->id])->get();

        return view('admin.questions.editQuestion' , compact('question' , 'answer' , 'subject' , 'data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Question::updateQuestion($request , $id);

        $correct_ans = Answer::updateAnswer($request , $id);

        foreach($correct_ans as $correct)
        {
            $data = $correct->id;
        }

        DB::table('questions')->where('id' ,'=' , $id)->update(['correct_answer_id' => $data]);

        Session::flash('success', 'Successfully Updated');

        return redirect('/admin/questions/');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $select = Question::find($id);

        $select->delete();

        Session::flash('error', 'Deleted Succefully');

        return redirect('/admin/questions');
    }
}
