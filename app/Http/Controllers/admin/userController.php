<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Session;
use Illuminate\Support\Facades\DB;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('role:admin');

    }

    public function index()
    {
        $users= User::where('role' , '!=' ,'user')->get();

        $roles = Role::all();

        return view('admin.subusers.index' , compact(['users' , 'roles']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.subusers.user' , compact('roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ins = new User;

        $this->validate($request , [

            'first_name'=> 'required',
            'last_name'=>'required',
            'email'=>'required|unique:users|email',
            'address'=>'required',
            'password'=>'required|min:6|confirmed',
            'phone_number'=>'required',
            'sex'=>'required',

        ]);

        $ins->entrance_id = $request->phone_number;

        $ins->firstname = $request->first_name;

        $ins->lastname = $request->last_name;

        $ins->email = $request->email;

        $ins->password = bcrypt($request->password);

        $ins->address = $request->address;

        $ins->phonenumber = $request->phone_number;

        $ins->sex = $request->sex;

        if(count($request->role)>1)
        {
            $ins->role = 'multiple roles';
        }
        else
        {
            foreach($request->role as $role)
            {
                $required_role = Role::find($role);

                $ins->role = $required_role->name;
            }
        }

        $ins->save();

        foreach($request->role as $role)
           {
               $ins->attachRole($role);
           }

        Session::flash('success', 'Successfully Added');

        return redirect('/admin/user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        $roles = Role::all();

        $roleid = DB::table('role_user')->select('role_id')->where('user_id' ,$id)->get();

        return view('admin.subusers.edit' , compact(['user' , 'roles' ,'roleid' ]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ins = User::find($id);

        $this->validate($request , [

            'first_name'=> 'required',
            'last_name'=>'required',
            'email'=>'required|email',
            'address'=>'required',
            'phone_number'=>'required',
            'sex'=>'required',

        ]);

        $ins->entrance_id = $request->phone_number;

        $ins->firstname = $request->first_name;

        $ins->lastname = $request->last_name;

        $ins->email = $request->email;

        $ins->address = $request->address;

        $ins->phonenumber = $request->phone_number;

        $ins->sex = $request->sex;

        if(count($request->role)>1)
        {
            $ins->role = 'multiple roles';
        }
        else
        {
            foreach($request->role as $role)
            {
                $required_role = Role::find($role);

                $ins->role = $required_role->name;
            }
        }

        $ins->save();

        DB::table('role_user')->where('user_id' , $ins->id)->delete();

        foreach($request->role as $role)
        {
            $ins->attachRole($role);
        }


        Session::flash('success', 'Successfully updated');

        return redirect('/admin/user');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();

        Session::flash('success', 'User deleted');

        return redirect('/admin/user');

    }
}
