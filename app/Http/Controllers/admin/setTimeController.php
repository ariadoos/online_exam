<?php

namespace App\Http\Controllers\admin;

use App\Settime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class setTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $data = Settime::all();

        return view('admin.time.setTime' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.time.addTime');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settime = Settime::rowCount($request);

        if($settime)
        {
            Session::flash('success' , "Time is set successfully");

            return redirect('/admin/time');
        }

        Session::flash('error' , "Time is already set");

        return redirect('/admin/time');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $time=Settime::find($id);

        return view('admin.time.editTime' , compact('time'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $time = new Settime;

        $tim= $time->updat($request , $id);

        if($tim)
        {
            $request->session()->flash('success' , "Updated Successfully");

            return redirect('/admin/time');
        }

        $request->session()->flash('error' , "Fail to Update");

        return redirect('/admin/time');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del=Settime::find($id);

        $del->delete();

        $data = Settime::all();

        $row = Settime::all()->count();

        Session::flash('error' , "Deleted Successfully");

        return redirect('/admin/time')->with('data' , 'row');
    }
}
