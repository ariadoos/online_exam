<?php

namespace App\Http\Controllers\admin;

use App\publish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Result;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;

class homeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $result = result::orderBy('scores', 'desc')->get();

        return view('admin.home', compact('result'));
    }

    public function publish()
    {
        $data = publish::all();

        return view('admin.publish.publish', compact('data'));
    }

    public function store(Request $request)
    {
        DB::table('publishes')->delete();

        $data = $request->decision;

        DB::table('publishes')->insert(['value' => $data]);

        Session::flash('success', "Successfull");

        return redirect('/admin/publish');
    }
}
