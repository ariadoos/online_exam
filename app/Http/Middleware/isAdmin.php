<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(($request->user()->role)=='user')
        {
            return $next($request);
        }


        // If user does not have the super-admin role redirect them to another page that isn't restricted

        return redirect('/admin');
    }
}
