<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function check($subject)
    {
        $subject = strtolower($subject);

        $result = self::where('subname' , '==' , $subject)->count();

        if($result==0)
        {
            return true;
        }

        return false;
    }

    public function updat($request , $id)
    {
        $sub = self::find($id);

        $sub->subname = $request->subName;

        $sub->weigh = $request->weigh;

        return $sub->save();

    }
}
