<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;
use App\Result;
use Illuminate\Support\Facades\DB;

class answeruser extends Model
{
    public static function store($request)
    {





        $obj = new answeruser();



        $id = $request->user()->id;

        $select = answeruser::where('user_id' , '=' , $id)->get();

        foreach($select as $sel)
        {
            DB::table('answerusers')->where('id' , '=' , $sel->id)->delete();
        }



        $question= Question::all();

        foreach($question as $ques)
        {

            $qid =$ques->id;





            $data [] = [
                'user_id' => $id,

                'question_id'=>$ques->id,

                'answer_id'=> $request->$qid,


            ];

        }

        $obj::insert($data);

        return true;


    }


    public static function checkAnswer($request)
    {

        $user_id = $request->user()->id;

        $ans_user = answeruser::all()->where('user_id' , '=' , $user_id);

        $total = 0;

        foreach($ans_user as $ans)
        {
            $qi= $ans->question_id;

            $check =  Question::find($qi);

            if($check->correct_answer_id == $ans->answer_id)
            {
                $total++;
            }

        }

        $result = array([
            'user_id' => $user_id ,
            'scores' => $total,
        ]);

        $obj = new Result;

        $obj::insert($result);

        return true;
    }
}
