<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Answer extends Model
{
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public static function addAnswer($request , $id)
    {

        DB::table('answers')->insert(array(
            array('question_id'=>$id , 'answer'=>$request->option1),
            array('question_id'=>$id , 'answer'=>$request->option2),
            array('question_id'=>$id , 'answer'=>$request->option3),
            array('question_id'=>$id , 'answer'=>$request->option4),
            ));


        $correct_ans = $request->correct_ans;

        if($correct_ans == 1)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option1)->get();

        }
        elseif($correct_ans == 2)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option2)->get();
        }
        elseif($correct_ans == 3)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option3)->get();
        }
        elseif ($correct_ans == 4)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option4)->get();
        }



    }

    public static function updateAnswer($request , $id)
    {
        $answer = Question::find($id)->answers;

        foreach($answer as $ans)
        {
            $ans = $ans->id ;
        }

        DB::table('answers')->where('id' , $ans-3)->update(['answer' => $request->option1] );

        DB::table('answers')->where('id' , $ans-2)->update(['answer' => $request->option2] );

        DB::table('answers')->where('id' , $ans-1)->update(['answer' => $request->option3] );

        DB::table('answers')->where('id' , $ans)->update(['answer' => $request->option4] );

        $correct_ans = $request->correct_ans;

        if($correct_ans == 1)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option1)->get();

        }
        elseif($correct_ans == 2)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option2)->get();
        }
        elseif($correct_ans == 3)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option3)->get();
        }
        elseif ($correct_ans == 4)
        {
            return DB::table('answers')->select(DB::raw('id'))->where('answer' , '=' , $request->option4)->get();
        }

    }

}
