<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public static function addQuestion($request)
    {
        $ques = new Question;

        $ques->question = $request->question;

        $ques->subject_id = $request->subject;

        $ques->save();

        return $ques->id ;
    }

    public static function updateQuestion($request , $id)
    {
         $ques = Question::find($id);

         $ques->question = $request->question;

         $ques->subject_id = $request->subject;

         return $ques->save();
    }


}
