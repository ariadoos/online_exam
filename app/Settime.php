<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settime extends Model
{
//    protected $dates   = [
//        'endingtime' , 'startingtime',
//    ];

    public static function rowCount($request)
    {
        $row=Self::all()->count();

        if($row<1) {

            $time = new Settime;

            $exam_date = $request->datepicker;

            $exam_date = date('Y-m-d' , strtotime($exam_date));

            $start_time = $request->timepicker1;

            $start_time = strtotime($start_time);

            $end_time = $request->timepicker2;

            $end_time =  strtotime($end_time);

            $time->examdate = $exam_date;

            $time->startingtime = $start_time;

            $time->endingtime = $end_time;

            $time->save();

            return true;
        }

        return false;

    }

    public static function updat($request , $id)
    {
        $time=Self::find($id);

        $exam_date = $request->datepicker;

        $exam_date = date('Y-m-d' , strtotime($exam_date));

        $start_time = $request->timepicker1;

        $start_time =strtotime($start_time);

        $end_time = $request->timepicker2;

        $end_time = strtotime($end_time);

        $time->examdate = $exam_date;

        $time->startingtime = $start_time;

        $time->endingtime = $end_time;

        return $time->save();

    }
}
