<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            [
                'name'=> 'role-read',
                'display_name'=>'Display Role Listing',
                'description'=> 'Sees only listing of Role',
            ],

            [
                'name'=> 'role-create',
                'display_name'=>'Create Role ',
                'description'=> 'Create New Role',
            ],
            [
                'name'=> 'role-edit',
                'display_name'=>'Edit Role ',
                'description'=> 'Edit Role',
            ],
            [
                'name'=> 'role-delete',
                'display_name'=>'Delete Role ',
                'description'=> 'Delete Role',
            ],
            [
                'name'=> 'create-question',
                'display_name'=>'Create Question',
                'description'=> 'Create Question',
            ],
            [
                'name'=> 'edit-question',
                'display_name'=>'Edit Question',
                'description'=> 'Edit Question',
            ],
            [
                'name'=> 'delete-question',
                'display_name'=>'Delete Question',
                'description'=> 'Delete Question',
            ],

            [
                'name'=> 'list-question',
                'display_name'=>'Display Questions listing',
                'description'=> 'See Questions listing',
            ],

            [
                'name'=> 'create-student',
                'display_name'=>'Create Student',
                'description'=> 'Create Student',
            ],
            [
                'name'=> 'edit-student',
                'display_name'=>'Edit Student',
                'description'=> 'Edit Student',
            ],
            [
                'name'=> 'delete-student',
                'display_name'=>'Delete Student',
                'description'=> 'Delete Student',
            ],

            [
                'name'=> 'list-student',
                'display_name'=>'Display Students listing',
                'description'=> 'See Students listing',
            ],

            [
                'name'=> 'create-subject',
                'display_name'=>'Create Subject',
                'description'=> 'Create Subject',
            ],
            [
                'name'=> 'edit-subject',
                'display_name'=>'Edit Subject',
                'description'=> 'Edit Subject',
            ],
            [
                'name'=> 'delete-subject',
                'display_name'=>'Delete Subject',
                'description'=> 'Delete Subject',
            ],

            [
                'name'=> 'list-subject',
                'display_name'=>'Display Subject listing',
                'description'=> 'See Subject listing',
            ],



        ];

        foreach($permission as $key=>$value)
        {
            App\Permission::create($value);
        }

    }

}
