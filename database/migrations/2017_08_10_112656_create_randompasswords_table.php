<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRandompasswordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('randompasswords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entrance_id');
//            $table->foreign('entrance_id')->references('entrance_id')->on('users')->onDelete('cascade');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('randowpasswords');
    }
}
