<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entrance_id')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('address');
            $table->unsignedBigInteger('phonenumber');
            $table->string('sex');
//            $table->enum('role',['admin','user'])->default('user');
            $table->string('role')->default('user');
            $table->timestamp('endtime')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
           'entrance_id'=>'98171',
           'firstname' =>'admin',
            'lastname'=>'adminn',
            'email'=>'admin@yahoo.com',
            'password'=>bcrypt("gces2014"),
            'address'=>'xxxx',
            'phonenumber'=>'988282',
            'sex'=>'x',
            'role'=>'admin',

        ]);




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
