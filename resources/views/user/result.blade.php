<!DOCTYPE html>
<html lang="en" >

<head>
	<meta charset="UTF-8">
	<title>Online Examination System | Landing Page</title>
	<link rel=icon" href="assets/first/css/logo3.png">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
	<link href={{asset("assets/css/student.css")}} media="screen" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href={{asset("assets/design/bootstrap.min.css")}}>
	<link rel="stylesheet" type="text/css" href={{asset("assets/design/style.css")}}>
	<link rel="stylesheet" type="text/css" href={{asset("assets/design/design200/style.css")}}>

	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'>

	{{--<link rel="stylesheet" href={{asset("assets/first/css/style.css")}}>--}}


</head>

<body>

<div class="navbar navbar-top navbar-inverse">

	<div class="navbar-inner">
		<div class=></div>
		<div class="container-fluid">


			<a class="brand" href="home.html"  >

				Online Examination Result


			</a>

			<ul class="nav pull-right">



				<li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top"><button type="button" class="btn btn-navbar"><i class="icon-align-justify"></i></button></li>

			</ul>

			<div class="nav-collapse nav-collapse-top collapse">

				<ul class="nav pull-right">







				</ul>
			</div>

		</div>

	</div>

</div>

<div id="sb-site" style="min-height: 1208px;"><div id="wrap">
	    <div class="main-inner-content">


	<div id="page-content">
	<div class="container mrg">
                    <div class="row">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span><a href="/home"><button type="button" class="btn btn-success">Go Back</button></a></span>
				<span style="float:left;">
				<form action={{route('logout')}} method='POST'>
					{{csrf_field()}}
					<button type="submit" class="btn btn-danger">Logout</button>
				</form>
				</span>

			<div class="widget">
			     <h3 class="widget-title" style="padding-bottom: 70px;">  </h3>

				<h1 class="widget-title"><span>You have Scored
                        @foreach($results as $res)
                            {{$res->scores}}

                        @endforeach


                    </span></h1>
			</div>
		</div>		
			<div class="table-responsive">
				<table class="table table-striped">
				<table class="table table-striped">
				<tbody><tr style="color:#fff;">
					<th style="background-color:#167f95;">Name</th>
					<th style="background-color:#167f95;">Total Question </th>
					<th style="background-color:#167f95;">Attempted Question</th>
					<th style="background-color:#167f95;">Total Correct</th>
					<th style="background-color:#167f95;">Total Marks</th>
				</tr>

				<tr>
					<td>
                        {{$user->firstname}}&nbsp{{$user->lastname}}
                    </td>
					<td>{{$totalQuestion}}</td>
					<td>{{$totalQuestion-$unsolved}}</td>
					<td>@foreach($results as $res)
                            {{$res->scores}}

                        @endforeach</td>
					<td>@foreach($results as $res)
                            {{$res->scores}}

                        @endforeach</td>
				</tr>
								</tbody></table></table>

			</div>
		</div>
	</div>
</div>		</div>
	</div>
	    </div>
	    </div>
	    </div>
	    </div>
	    </body>
	    </head>
	    </html>
	    


	
 
