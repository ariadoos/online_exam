<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <link rel="stylesheet" href={{asset("assets/css/centerfont.css")}}>
    <link href={{asset("assets/jquery/jquery-ui-1.10.3.custom.css")}} media="screen" rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/student.css")}} media="screen" rel="stylesheet" type="text/css">
    <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
    <script src={{asset("assets/js/all.js")}} type="text/javascript"></script>
    <title>Online Examination System</title>
</head><body>
<div id="main_body">

    <div class="navbar navbar-top navbar-inverse">

        <div class="navbar-inner">

            <div class="container-fluid">

                <a class="brand" href="home.html">

                    Online Examination System
                </a>

                <ul class="nav pull-right">



                    <li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top">
                        <form action={{route('logout')}} method='POST'>
                    {{csrf_field()}}
                    <li class="nav-item">
                        <input type="submit"  style="

        color: black;

    "
                               class="btn btn-danger" value="logout">
                    </li>
                    </form>


                    </li>

                </ul>

                <div class="nav-collapse nav-collapse-top collapse">

                    <ul class="nav pull-right">


                    </ul>
                </div>

            </div>

        </div>

    </div>

    <div class="main-content">
        <br>
        <div class="container-fluid padded">
            <div class="container-fluid padded">

                <div class="row-fluid">
                    <div class="span8">
                        <div class="box">
                            <div class="box-header">
                                <span class="title"><i class="icon-reorder"></i>&nbsp;Questions</span>
                            </div>
                            <div class="box-content scrollable" style="max-height: 500px; overflow-y: auto; overflow-x: auto">
                                <div class="box-section news with-icons">
                                    <form method="post" id="quiz_form" action="/home/submit">
                                        {{csrf_field()}}
                                        <table style="width:100%;vertical-align:top" class="table table-normal">
                                            <tbody><tr>
                                                <td style="vertical-align:top">
                                                    <div id="1_1" class="display_question">

                                                       @if($check == 0)

                                                        @foreach($subject as $sub)
                                                                @php($take = $sub->weigh)

                                                                @php($question =  App\Question::where('subject_id' , $sub->id)->inRandomOrder()->take($take)->get())

                                                                    @php($count = $question->count())

                                                                        @if($count != 0)

                                                                            <h3>{{$sub->subname}}</h3>

                                                                        @endif


                                                        @php($i=1)


                                                        @foreach($question as $ques)

                                                            <?php


                                                             DB::table('answerusers')->insert(['user_id' => $id , 'question_id' => $ques->id]);

                                                             ?>


                                                        <div class="bradcome-menu qu-pa">
                                                            <div class="col-md-6"> <span class="question"> Question No. {{$i}}</span></div>
                                                        </div>



                                                        <table class="answeers" border="0" width="100%">
                                                            <tbody>
                                                            <tr>

                                                                <h4 class="quction"><p>{{$ques->question}}</p></h4>
                                                            </tr>

                                                            @php($answer = App\Question::find($ques->id)->answers)
                                                            @foreach($answer as $ans)
                                                            <tr>
                                                                <td style="width:10px">
                                                                    <input name="{{$ques->id}}" value="{{$ans->id}}"  type="radio"></td>
                                                                <td>{{$ans->answer}}</td>
                                                            </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>

                                                        <br>
                                                        <hr>
                                                            @php($i++)
                                                                @endforeach

                                                        @endforeach
                                                    </div>

                                                </td>
                                            </tr>



                                            @else

                                                @php($sub = NULL)

                                            @foreach($question as $ques)

                                            @php($subject = App\Question::find($ques->question_id)->subject)

                                            @if($subject->subname != $sub)

                                                @php($sub = $subject->subname)
                                                <h3>{{$sub}}</h3>
                                            @endif

                                            @php($i=1)


                                                        <div class="bradcome-menu qu-pa">
                                                                    <div class="col-md-6"> <span class="question"> Question No. {{$i}}</span></div>
                                                                </div>



                                                                <table class="answeers" border="0" width="100%">
                                                                    <tbody>
                                                                    @php($quest = App\Question::find($ques->question_id))
                                                                    <tr>

                                                                        <h4 class="quction"><p>{{$quest->question}}</p></h4>
                                                                    </tr>

                                                                    @php($answer = App\Question::find($quest->id)->answers)
                                                                        @foreach($answer as $ans)
                                                                            <tr>
                                                                                <td style="width:10px">
                                                                                    <input name="{{$ques->id}}" value="{{$ans->id}}" class="option-input radio" type="radio"></td>
                                                                                <td>{{$ans->answer}}</td>
                                                                            </tr>
                                                                        @endforeach


                                                                    </tbody>
                                                                </table>

                                                                <br>
                                                                <hr>
                                    @php($i++)
                                        @endforeach


                                </div>

                                </td>
                                </tr>
                                @endif


                                            <tr>
                                                <td>
                                                    <table>
                                                        <tbody><tr>
                                                            <td><div id="clearAnswer" class="btn btn-gray">Clear Answer</div></td>
                                                            <td><div style="float:right"><input id="finish" class="btn btn-green" value="Finish" name="Finish" onclick="return confirm('Are you sure you want to submit the test?')" type="submit"></div></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--question part--->

                <!---timer part-->
                <div class="span4">
                    <div class="box">
                        <div class="box-header">
                            <span class="title"><i class="icon-question-sign"></i>&nbsp Time Left</span>
                        </div>
                        <div class="box-content scrollable" style="max-height: 600px;  overflow-y: auto">
                            <div class="box-section news with-icons">
                                <table align="center" width="100%" border="0" valign="top" style="vertical-align:top" class="table table-normal">
                                    <tbody>
                                    <tr>
                                        <td colspan="2" class="" id="timerdiv"><span id="mins"></span>:<span id="seconds"></span></td>
                                    </tr>
                                    </tbody></table>
                                <div>&nbsp;</div>
                                <div class="g-awareness">

                                </div>
                                <div>&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---Timer part-->

            </div>
            </div>

        </div>
    </div>



<script>
    $(document).ready(function(){


        document.onkeydown = function (e) {
            return (e.which || e.keyCode) != 116;
        };

        $(document).bind('contextmenu',function(e){
            e.preventDefault();
        });

        updateSummary();
        $(".display_question").each(function(e) {
            if (e != 0)
                $(this).hide();
        });

        $("#next").click(function(){
            $("#prev").show();
            var  numberPlateId ="#"+"number-"+$(".display_question:visible").attr('id');

            if(($(".display_question:visible table input:radio:checked").length == 0)&&($(".display_question:visible table input:checkbox:checked").length == 0))
                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('n-answered');
            else
                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('q-answered');
            if ($(".display_question:visible").next().length != 0)
                $(".display_question:visible").next().fadeIn(1000).prev().hide();
            else {

                $(this).hide();
            }
            updateSummary();
            return false;
        });

        $("#mnext").click(function(){
            $("#prev").show();

            var  numberPlateId ="#"+"number-"+$(".display_question:visible").attr('id');


            if(($(".display_question:visible table input:radio:checked").length == 0)&&($(".display_question:visible table input:checkbox:checked").length == 0))
            {

                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('n-answered');
            }
            else
            {

                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('m-answered');
            }


            if ($(".display_question:visible").next().length != 0)
                $(".display_question:visible").next().fadeIn(1000).prev().hide();
            else {
                $(this).hide();
            }
            updateSummary();
            return false;
        });

        $("#prev").click(function(){
            $("#next").show();
            if ($(".display_question:visible").prev().length != 0)
                $(".display_question:visible").prev().fadeIn(1000).next().hide();
            else {

                $(this).hide();
            }
            updateSummary();
            return false;
        });

        $("#clearAnswer").click(function(){
            //$("#next").show();
            var  numberPlateId ="#"+"number-"+$(".display_question:visible").attr('id');

            if($(".display_question:visible table input:radio:checked").length != 0)
            {
                $(".display_question:visible table input:radio:checked").removeAttr("checked");
                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('z-answered');
            }
            else if ($(".display_question:visible table input:checkbox:checked").length != 0)
            {
                $(".display_question:visible table input:checkbox:checked").removeAttr("checked");
                $(numberPlateId).removeClass('q-answered m-answered n-answered z-answered').addClass('z-answered');
            }
            else
            {
                alert('No Answer Selected');
            }

            updateSummary();
            return false;
        });

        var mins=60;
        var sec = 60;

        intilizetimer();

    });

    function showSubjectQuestions(id){
        $('.display_question').hide();
        $('#'+id+'_1').fadeIn(1000);
    }

    function showQuestion(id){

        $('.display_question').show();
        $(id).fadeIn(1000);
    }

    function updateSummary()
    {
        $(".am-answered").text($(".number-plate .m-answered").length);
        $(".az-answered").text($(".number-plate .z-answered").length);
        $(".an-answered").text($(".number-plate .n-answered").length);
        $(".aq-answered").text($(".number-plate .q-answered").length);
    }


    function intilizetimer()
    {
        //totaltime = $("#totaltime").text().split(":");
        mins ={{$var}}; //totaltime[0];
        sec = '0';//totaltime[1];
        startInterval();
    }

    function tictac(){
        sec--;
        if(sec<=0)
        {
            mins--;
            $("#mins").text(mins);
            if(mins<1)
            {
                $("#timerdiv").css("color", "red");
            }
            if(mins<0)
            {
                stopInterval();
                $("#mins").text('0');
                alert('You are exceeded the time to finish the exam.');
                $('#finish').removeAttr('onclick');
                $('#finish').click();
                $('form').submit();

            }


            sec=60;
        }
        if(mins>=0)
            $("#seconds").text(sec);
        else
            $("#seconds").text('00');
    }
    function startInterval()
    {
        timer= setInterval("tictac()", 1000);
    }
    function stopInterval()
    {
        clearInterval(timer);
    }
</script>








    <div style="clear:both;color:#aaa; padding:20px;">

        <center>

            © 2017  <a href="#" target="_blank">Online Examination System</a>

        </center>

    </div>
</div>


</body></html>