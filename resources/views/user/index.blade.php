<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Online Examination System | Landing Page</title>
    <link rel=icon" href="assets/first/css/logo3.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <link href={{asset("assets/css/student.css")}} media="screen" rel="stylesheet" type="text/css">


    <link rel='stylesheet prefetch'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'>

    <link rel="stylesheet" href={{asset("assets/first/css/style.css")}}>


</head>

<body>

<div class="navbar navbar-top navbar-inverse">

    <div class="navbar-inner">
        <div class=></div>
        <div class="container-fluid">


            <a class="brand" href="home.html">

                Online Examination System


            </a>

            <ul class="nav pull-right">


                <li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top">
                    <button type="button" class="btn btn-navbar"><i class="icon-align-justify"></i></button>
                </li>

            </ul>

            <div class="nav-collapse nav-collapse-top collapse">

                <ul class="nav pull-right">

                    <li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top">
                        <form action={{route('logout')}} method='POST'>
                    {{csrf_field()}}
                    <li class="nav-item">
                        <input type="submit" style="color: black;"
                               class="btn btn-danger" value="logout">
                    </li>
                    </form>

                    </li>

                </ul>
            </div>

        </div>

    </div>

</div>

<div class="card-container">


    @if(($result_row == 0) && ($current_date == $date) && (($current_time >= $stime) && ($current_time<=$etime)) )
        <a href="/home/display" style="text-decoration: none;  ">
            <div class="card">
                <div class="card-image"></div>
                <div class="card-info">
                    <div class="card-title">Take Exam</div>
                </div>

            </div>
        </a>
    @else
        <a href="#" style="text-decoration: none;cursor:not-allowed; " title="You are not allowded ">
            <div class="card">
                <div class="card-image"></div>
                <div class="card-info">
                    <div class="card-title">Take Exam</div>
                </div>

            </div>
        </a>
    @endif



    @foreach($publish_check as $publish)

        @if($result_row == 1 && ($publish->value == 1))
            <a href="/home/result" style="text-decoration: none; @if($result_row == 0) cursor:not-allowed; @endif">
                <div class="card">
                    <div class="card-image2"></div>
                    <div class="card-info">
                        <div class="card-title">Result</div>

                    </div>

                </div>
            </a>
        @elseif($publish->value == 0 || $result_row == 0)
            <a href="#" style="text-decoration: none; cursor:not-allowed; " title="Result Not Published Yet">
                <div class="card">
                    <div class="card-image2"></div>
                    <div class="card-info">
                        <div class="card-title">Result</div>

                    </div>

                </div>
            </a>

        @endif
    @endforeach

</div>


</body>

</html>
