<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Thank you</title>
      <link rel="stylesheet" href={{asset("assets/css/style.css")}}>
      <link href={{asset("assets/css/student.css")}} media="screen" rel="stylesheet" type="text/css">


  
</head>

<body>

  <div class="navbar navbar-top navbar-inverse">
 
  <div class="navbar-inner">
     
    <div class="container-fluid">
              <!--  <img src="logo5.png" border="0" style="width:100px; height:100px;"> -->

      <a class="brand" href="home.html" >

      
            Online Examination System

      </a>
<!-- 
      <ul class="nav pull-right">
    
        

        <li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top"><button type="button" class="btn btn-navbar"><i class="icon-align-justify"></i></button></li>

      </ul> -->

      <div class="nav-collapse nav-collapse-top collapse">

                <ul class="nav pull-right">

                    <li class="hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top">
                        <form action={{route('logout')}} method='POST'>
                    {{csrf_field()}}
                    <li class="nav-item">
                        <input type="submit" class="btn btn-danger" value="logout">
                    </li>
                    </form>


                    </li>


                </ul>
      </div>

    </div>

  </div>

   </div> 
  <div class=content>
 
    <div class="wrapper-1">
      <h1>Thank you !</h1>
      <p>Good luck for your success...  </p>
      <a href="/home"><button class="go-home">
      go home
      </button></a>
    </div>

</div>

</body>

</html>
