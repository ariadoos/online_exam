<h1>Result list</h1>


<table border="1" width="100%" cellpadding="10">
    <thead>
    <tr>
        <th>S.N.</th>
        <th>Student Name</th>
        <th>Total Questions</th>
        <th>Attempted Questions</th>
        <th>Obtained Marks</th>

    </tr>
    </thead>
    <tbody>
    @php($result = App\result::orderBy('scores' , 'desc')->get())
        @php($i=1)
            @foreach($result as $res)
                <tr>
                    @php($user = App\User::find($res->user_id))
                        @php($totalQuestion = App\answeruser::where('user_id' , '=' , $user->id)->count())
                            @php($unsolved =App\answeruser::where('answer_id' , '=' , NULL)->where('user_id' , '=' , $user->id)->count())
                                <td>{{$i}}</td>
                                <td>{{ucfirst($user->firstname)}} {{ucfirst($user->lastname)}}</td>
                                <td>
                                    {{$totalQuestion}}
                                </td>
                                <td>{{$totalQuestion-$unsolved}}</td>
                                <td>{{$res->scores}}</td>
                </tr>
    @php($i++)

    @endforeach
    </tfoot>
</table>