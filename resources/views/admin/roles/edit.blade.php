<!-- header section -->

@include("admin.header")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Students
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/role"><i class="fa fa-dashboard">Role Management</i></a></li>
            <li><a href="/admin/role">Roles</a></li>
            <li class="active">Edit Role</li>
        </ol>
    </section>

@include('errors.error')


<!-- Main content -->
    <section class="content">


        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <h2 class="box-title">Edit Role</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/role"><button class="btn btn-warning pull-right">Goback</button></a>

                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action={{'/admin/role/'.$role->id}}>
                        {{csrf_field()}}
                        {{method_field("PUT")}}
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Name</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" value="{{$role->name}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Display Name</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="display_name" value="{{$role->display_name}}">
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" id="comment" name="description"  required>{{$role->description}}</textarea>
                            </div>
                        </div>


                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Permission</label>
                        </div>

                        <div class="form-group row col-sm-3">

                            @foreach($permissions as $permission)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" {{in_array($permission->id , $role_permission)?"checked":""}} name="permission[]" value="{{$permission->id}}">
                                        {{$permission->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                        <div class="line"></div>

                        <div class="form-group row col-sm-12 ">
                            <div class="col-sm-4 offset-sm-3 pull-right">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->







<!-- footer section -->

@include("admin.footer")