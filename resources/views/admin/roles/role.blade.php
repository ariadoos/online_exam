@include('admin.header')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Role Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/role"><i class="fa fa-dashboard"></i>Role Management</a></li>
            <li class="active">Roles</li>
        </ol>
    </section>

@include('errors.error')
<!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">Roles</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/role/create"><button class="btn btn-success pull-right">
                                <i class="fa fa-plus">Add Role</i></button></a>

                    </div>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Role</th>
                            <th>Description</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$role->display_name}}</td>
                                <td>{{$role->description}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href=""><button type="button" class="btn btn-warning" title="View Role"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="{{'/admin/role/'.$role->id.'/edit'}}"><button type="button" class="btn btn-info" title="Edit Role"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                     </div>
                                    <div class="btn-group">
                                        <form method="POST" action="{{'/admin/role/'.$role->id}}">
                                            {{csrf_field()}}
                                            {{method_field("DELETE")}}
                                            <button type="submit" class="btn btn-danger" title="Delete Role" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                        @php($i++)
                        @endforeach
                        </tbody>
                    </table>
                </div>


                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->






@include('admin.footer')