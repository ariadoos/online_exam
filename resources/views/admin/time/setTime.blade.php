<!-- header section -->

@include("admin.header")




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Exam Time
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/time"><i class="fa fa-dashboard"></i>Exam Time info.</a></li>
            <li class="active">View Time</li>
        </ol>
    </section>

@include('errors.error')

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-xs-2">
                    <span><i class="fa fa-graduation-cap"></i></span>
                    <h2 class="box-title">Exam Time Info.</h2>
                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-2">
                    <a href="/admin/time/create"><button class="btn btn-success pull-right">
                            <i class="fa fa-plus">Set Time</i></button></a>

                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="box-header">

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Exam Date (Y-m-d)  </th>
                        <th>Starting Time</th>
                        <th>Ending Time</th>
                        <th>Edit</th>
                        <th>Delete</th>

                    </tr>
                    </thead>
                    <tbody>
                    @php($i = 1)

                        @foreach($data as $d )

                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$d->examdate}}</td>
                                <td>{{date("g:i a" , $d->startingtime)}}</td>
                                <td>{{date("g:i a" , $d->endingtime)}}</td>

                                <td style="text-align: center;">
                                    <div class="btn-group">
                                        <a href="{{'/admin/time/'.$d->id.'/edit'}}"><button type="button" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                    </div>
                                </td>

                                <td style="text-align: center;">
                                    <div class="btn-group">
                                        <form method="POST" action="{{'/admin/time/'.$d->id}}">
                                            {{csrf_field()}}
                                            {{method_field("DELETE")}}
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </form>

                                    </div>

                                </td>

                            </tr>

                            @php($i++)


                                @endforeach



                    </tbody>
                </table>
            </div>
            <!-- /.box-body small-->

            <div class="box-footer">
            </div>
            <!-- /.box-footer-->
        </div>
        {{--box body big--}}
    </div>
    <!-- /.default box -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->






<!-- footer section -->

@include("admin.footer")