<!-- header section -->

@include("admin.header")

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Time
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admim/time"><i class="fa fa-dashboard"></i>Time</a></li>
            <li><a href="/admin/time">View Time</a></li>
            <li class="active">Set Time</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">Set Time</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/time" ><button class="btn btn-primary pull-right">Go back</button></a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <form class="form-horizontal" method="POST" action="/admin/time">
                    {{csrf_field()}}
                    <!-- Date -->
                    <div class="form-group">
                        <label>Exam Date:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="datepicker" id="datepicker" required>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->

                    <!-- time Picker -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label>Starting Time</label>

                            <div class="input-group">
                                <input type="text" class="form-control timepicker" name="timepicker1" required>

                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- time picker end-->

                    <!-- time Picker -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label>Ending Time:</label>

                            <div class="input-group">
                                <input type="text" class="form-control timepicker" name="timepicker2" required>

                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- time picker end-->
                    <div class="form-group row">
                        <div class="col-sm-4 offset-sm-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>

                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->





<!-- footer section -->

@include("admin.footer")