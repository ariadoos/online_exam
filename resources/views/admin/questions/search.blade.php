<!-- header section -->

@include("admin.header")

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Questions
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/questions"><i class="fa fa-dashboard"></i>Questions</a></li>
            <li>{{ucfirst($subj->subname)}} Questions List</li>
        </ol>
    </section>

@include('errors.error')


<!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box ">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-4">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">{{ucfirst($subj->subname)}} Questions List</h2>
                    </div>

                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        @permission('create-question')
                        <a href="/admin/questions/create" ><button class="btn btn-success pull-right"><i class="fa fa-plus">Set Questions</i></button></a>
                        @endpermission
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                    <form class="form-horizontal" method="GET" action="/admin/questions/">
                        <div class="col-xs-1" style="text-align: center; padding-top: 6px;">
                            <label class="form-control-label">Sort by:</label>
                        </div>
                        <div class="col-xs-4">
                            <select name="subject" class="form-control">
                                <option value="" disabled selected hidden>Please Choose Subject...</option>
                                <option value="all">All</option>
                                @foreach($subject as $sub)
                                    <option value="{{$sub->id}}">{{$sub->subname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-2">
                            <button type="submit" class="btn btn-primary">Go</button>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-2"></div>

                    </form>


                </div>
                <!-- /.box-header -->

                <div class="line"></div>


                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr id="tableHeader">
                            <th>S.N</th>
                            <th>Questions</th>
                            <th>Options 1</th>
                            <th>Options 2</th>
                            <th>Options 3</th>
                            <th>Options 4</th>
                            <th>Correct Answer</th>
                            @permission('edit-question')<th>Edit</th>@endpermission
                            @permission('delete-question')<th>Delete</th>@endpermission
                        </tr>
                        </thead>
                        <tbody>
                        @permission('list-question')
                        @php($i=1)


                            @php($id = $subj->id)

                                @php($question=App\Subject::find($id)->questions)



                                    @foreach($question as $ques)

                                        <tr>
                                            <th scope="row">{{$i}}</th>

                                            <td>{{$ques->question}}

                                            </td>

                                            @php($answer=App\Question::find($ques->id)->answers)

                                                @foreach($answer as $ans)

                                                    <td>{{$ans->answer}}</td>

                                                @endforeach
                                                <td>
                                                    @php($ans=App\Answer::find($ques->correct_answer_id))

                                                        {{$ans->answer}}

                                                </td>

                                                @php($i++)

                                                        @permission('edit-question')
                                                        <td>
                                                            <div class="btn-group">

                                                                <a href="{{'/admin/questions/'.$ques->id.'/edit'}}"><button type="button" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                                            </div>
                                                        </td>
                                                        @endpermission

                                                        @permission('delete-question')
                                                        <td>
                                                            <div class="btn-group">
                                                                <form method="POST" action="{{'/admin/questions/'.$ques->id}}">
                                                                    {{csrf_field()}}
                                                                    {{method_field("DELETE")}}
                                                                    <button type="submit" class="btn btn-danger"   onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                                                </form>

                                                            </div>

                                                        </td>
                                                        @endpermission

                                        </tr>

                                    @endforeach


                        @endpermission
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- footer section -->

@include("admin.footer")