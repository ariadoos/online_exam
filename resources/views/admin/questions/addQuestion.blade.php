<!-- header section -->

@include("admin.header")



<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Questions
            </h1>
            <ol class="breadcrumb">
                <li><a href="/admin/questions"><i class="fa fa-dashboard"></i>Questions</a></li>
                <li><a href="/admin/questions">View Questions</a></li>
                <li class="active">Add Question</li>
            </ol>
        </section>
    @include('errors.error')
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-xs-2">

                            <h2 class="box-title">Add Questions</h2>
                        </div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-2"></div>
                        <div class="col-xs-2">
                            <a href="/admin/questions"><button class="btn btn-primary pull-right">Go Back</button></a>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" action="/admin/questions">
                            {{csrf_field()}}
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Select Subject</label>
                                <div class="col-sm-9 select">
                                    <select  required name="subject" class="form-control" style="cursor:pointer" >
                                        <option value="" disabled selected hidden>Please Choose Subject...</option>
                                    @foreach($data as $sub)
                                        <option value="{{$sub->id}}">{{$sub->subname}}</option>
                                    @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="line"></div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Questions</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="5" id="comment" name="question"  required></textarea>
                                </div>
                            </div>

                            <div class="line"></div>

                            <!--options of the questions-->
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Options</label>
                                <div class="col-sm-9">

                                    <div class="form-group">
                                        <div class="input-group"><span class="input-group-addon">
                                                <input id="radioCustom1" type="radio" value="1" name="correct_ans"  style="cursor:pointer"class="radio-template" required></span>
                                            <input type="text" name="option1" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group"><span class="input-group-addon">
                                                <input id="radioCustom1" type="radio" value="2" name="correct_ans"  style="cursor:pointer"class="radio-template"></span>
                                            <input type="text" name="option2" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group"><span class="input-group-addon">
                                                <input id="radioCustom1" type="radio" value="3" name="correct_ans"  style="cursor:pointer"class="radio-template"></span>
                                            <input type="text" name="option3" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group"><span class="input-group-addon">
                                                <input id="radioCustom1" type="radio" value="4" name="correct_ans" style="cursor:pointer" class="radio-template"></span>
                                            <input type="text" name="option4" class="form-control" required >
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="line"></div>


                            <div class="form-group row">
                                <div class="col-sm-4 offset-sm-3">
                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body small-->

                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer-->
                </div>
                {{--box body big--}}
            </div>
            <!-- /.default box -->


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



    <!-- footer section -->

@include("admin.footer")