<!-- header section -->

@include("admin.header")



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Questions
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/questions"><i class="fa fa-dashboard"></i>Questions</a></li>
            <li><a href="/admin/questions">View Questions</a></li>
            <li class="active">Edit Question</li>
        </ol>
    </section>
@include('errors.error')
<!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">

                        <h2 class="box-title">Edit Questions</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/questions"><button class="btn btn-primary pull-right">Go Back</button></a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="{{'/admin/questions/'.$question->id}}">
                        {{csrf_field()}}
                        {{method_field("PUT")}}
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Select Subject</label>
                            <div class="col-sm-9 select">
                                <select  required name="subject" class="form-control" style="cursor:pointer" >
                                    <option value="" disabled selected hidden>Please Choose Subject...</option>
                                    <option value="{{$subject->id}}" selected="selected">{{$subject->subname}}</option>
                                    @foreach($data as $sub)
                                        <option value="{{$sub->id}}">{{$sub->subname}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Questions</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" id="comment" name="question"  required>{{$question->question}}</textarea>
                            </div>
                        </div>

                        <div class="line"></div>

                        <!--options of the questions-->
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Options</label>
                            <div class="col-sm-9">


                                @php($j=1)

                                    @foreach($answer as $ans)

                                        <div class="form-group">
                                            <div class="input-group"><span class="input-group-addon">
                                                <input id="radioCustom1" type="radio" value={{$j}} name="correct_ans"  style="cursor:pointer"class="radio-template" required
                                                    @if($question->correct_answer_id == $ans->id)
                                                       checked="checked"
                                                    @endif
                                                ></span>
                                                <input type="text" name="{{'option'.$j}}"  value={{$ans->answer}} class="form-control" required>
                                            </div>
                                        </div>
                                        @php($j++)
                                    @endforeach

                            </div>
                        </div>

                        <div class="line"></div>


                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- footer section -->

@include("admin.footer")