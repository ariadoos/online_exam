<footer class="main-footer">

    <strong>Copyright &copy; 2017.</strong> All rights
    reserved.
</footer>


<script src={{asset("bower_components/jquery/dist/jquery.min.js")}}></script>
<!-- Bootstrap 3.3.7 -->
<script src={{asset("bower_components/bootstrap/dist/js/bootstrap.min.js")}}></script>
<!-- DataTables -->
<script src={{asset("bower_components/datatables.net/js/jquery.dataTables.min.js")}}></script>
<script src={{asset("bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}></script>
<!-- SlimScroll -->
<script src={{asset("bower_components/jquery-slimscroll/jquery.slimscroll.min.js")}}></script>
<!-- FastClick -->
<script src={{asset("bower_components/fastclick/lib/fastclick.js")}}></script>
<!-- AdminLTE App -->
<script src={{asset("dist/js/adminlte.min.js")}}></script>
<!-- AdminLTE for demo purposes -->
<script src={{asset("dist/js/demo.js")}}></script>
<!-- Select2 -->
<script src={{asset("bower_components/select2/dist/js/select2.full.min.js")}}></script>

<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    });
</script>

<!-- bootstrap datepicker -->
<script src={{asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}></script>

<!-- bootstrap time picker -->

<script src={{asset("plugins/timepicker/bootstrap-timepicker.min.js")}}></script>



<script>
    $(function () {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //Time picker
        $('.timepicker').timepicker({
            showInputs: false
        })
        //Initialize Select2 Elements
        $('.select2').select2()
    });
</script>



</body>
</html>
