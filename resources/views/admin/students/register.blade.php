<!-- header section -->

@include("admin.header")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Students
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/students"><i class="fa fa-dashboard">Students</i></a></li>
            <li><a href="/admin/students">View Student</a></li>
            <li class="active">Add Student</li>
        </ol>
    </section>

    @include('errors.error')


    <!-- Main content -->
    <section class="content">


        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        <h2 class="box-title">Add Student</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/students"><button class="btn btn-warning pull-right">Goback</button></a>

                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="/admin/students">
                    {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Entrance Number</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="entrance_number" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">First Name</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Last Name</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Email</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Address</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Phone Number</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone_number" required>
                                </div>
                            </div>
                        </div>

                        <div class="line"></div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Sex</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="radio-inline"><input type="radio" name="sex" value="male" required>Male</label>
                                    <label class="radio-inline"><input type="radio" name="sex" value="female">Female</label>
                                    <label class="radio-inline"><input type="radio" name="sex" value="others" >Others</label>
                                </div>

                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <button type="reset" class="btn btn-secondary">Reset</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->







<!-- footer section -->

@include("admin.footer")