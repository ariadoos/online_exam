<!-- header section -->

@include("admin.header")

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Students
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/students"><i class="fa fa-dashboard"></i>Students</a></li>
            <li>View Students</li>
        </ol>
    </section>

    @include('errors.error')
    <!-- Main content -->

    @permission('list-student')
    <section class="content">

        <!-- Default box -->
        <div class="box ">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">View Students</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        @permission('create-student')
                        <a href="/admin/students/create" ><button class="btn btn-success pull-right"><i class="fa fa-plus">Add Student</i></button></a>
                        @endpermission
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr id="tableHeader">
                            <th>S.N</th>
                            <th>Entrance Number</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Sex</th>
                            @permission('edit-student')<th>Edit</th>@endpermission
                            @permission('delete-student')<th>Delete</th>@endpermission


                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)

                        @foreach($users as $user )

                            <tr>
                                <th scope="row">{{$i}}</th>
                                <td>{{$user->entrance_id}}</td>
                                <td>{{$user->firstname}}</td>
                                <td>{{$user->lastname}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->address}}</td>
                                <td>{{$user->phonenumber}}</td>
                                <td>{{$user->sex}}</td>


                                @permission('edit-student')
                                <td>
                                    <div class="btn-group">
                                        <a href="{{'/admin/students/'.$user->id.'/edit'}}"><button type="button" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                    </div>
                                </td>
                                @endpermission


                                @permission('delete-student')
                                <td>
                                    <div class="btn-group">
                                        <form method="POST" action="{{'/admin/students/'.$user->id}}">
                                            {{csrf_field()}}
                                            {{method_field("DELETE")}}
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </form>

                                    </div>

                                </td>
                                @endpermission

                            </tr>

                            @php($i++)


                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
    @endpermission
</div>
<!-- /.content-wrapper -->


<!-- footer section -->

@include("admin.footer")
