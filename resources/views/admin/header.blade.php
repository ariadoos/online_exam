<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Examination</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href={{asset("bower_components/bootstrap/dist/css/bootstrap.min.css")}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{asset("bower_components/font-awesome/css/font-awesome.min.css")}}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{asset("bower_components/Ionicons/css/ionicons.min.css")}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("dist/css/AdminLTE.min.css")}}>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href={{asset("dist/css/skins/_all-skins.min.css")}}>

    <!-- iCheck -->
    <link rel="stylesheet" href={{asset("plugins/iCheck/square/blue.css")}}>

    <!-- bootstrap datepicker -->
    <link rel="stylesheet"
          href={{asset("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}>

    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href={{asset("plugins/timepicker/bootstrap-timepicker.min.css")}}>

    <!-- Select2 -->
    <link rel="stylesheet" type="text/css" href={{asset("bower_components/select2/dist/css/select2.min.css")}}>

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href={{route('admin')}} class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>O.</b>Ex.</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Online</b>Examination</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu" style="padding:6px;">
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    <!-- Logout    -->
                    <form action={{route('logout')}} method='POST'>
                        {{csrf_field()}}
                        <li class="nav-item">
                            <input type="submit" class="btn btn-danger" value="logout">
                        </li>
                    </form>

                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src={{asset("dist/img/avatar5.png")}} class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ucfirst(Auth::user()->firstname)}}</p>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>

                <li>
                    <a href={{route('admin')}}>
                        <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>

                    </a>
                </li>

                @permission(array('create-student' ,'edit-student','delete-student','list-student'))
                <li>
                    <a href="/admin/students">
                        <i class="fa fa-users" aria-hidden="true"></i> <span>Students</span>
                    </a>
                </li>
                @endpermission

                @permission(array('create-question' ,'edit-question' ,'delete-question' ,'list-question'))
                <li>
                    <a href="/admin/questions">
                        <i class="fa fa-quora" aria-hidden="true"></i><span>Questions</span>

                    </a>
                </li>
                @endpermission

                @permission(array('create-subject' ,'edit-subject' ,'delete-subject' , 'list-subject'))
                <li>
                    <a href="/admin/subject">
                        <i class="fa fa-book" aria-hidden="true"></i> <span>Subject</span>

                    </a>
                </li>
                @endpermission

                @role('admin')

                <li>
                    <a href="/admin/time ">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> <span>Set Time</span>

                    </a>
                </li>


                <li>
                    <a href="/admin/role ">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> <span>Role Management</span>
                    </a>
                </li>


                <li>
                    <a href="/admin/user ">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> <span>Add User</span>
                    </a>
                </li>


                <li>
                    <a href="/admin/download " target="_blank">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <span>Random list</span>

                    </a>
                </li>

                <li>
                    <a href="/admin/publish ">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> <span>Publish Result</span>

                    </a>
                </li>

                @endrole

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

