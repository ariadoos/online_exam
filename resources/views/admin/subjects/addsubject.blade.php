<!-- header section -->

@include("admin.header")



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subject
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/subject"><i class="fa fa-dashboard"></i>Subject</a></li>
            <li><a href="/admin/subject">Subject List</a></li>
            <li class="active">Add Subject</li>
        </ol>
    </section>

    @include('errors.error')
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">Add Subject</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        <a href="/admin/subject" ><button class="btn btn-primary pull-right">Go back</button></a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" action="/admin/subject" method="POST">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Subject Name</label>
                            <div class="col-sm-9 select">
                                <input type="text" class="form-control" name="subName" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Total Question (1marks for each question)</label>
                            <div class="col-sm-9 select">
                                <input type="text" class="form-control" name="weigh" required>
                            </div>
                        </div>

                        <div class="line"></div>


                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <button type="reset" class="btn btn-secondary">Reset</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- footer section -->

@include("admin.footer")
