<!-- header section -->

@include("admin.header")


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subject
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Subject</a></li>
            <li class="active">Subject List</li>
        </ol>
    </section>

    @include('errors.error')
    <!-- Main content -->

    @permission('list-subject')
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">Subject List</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">

                        @permission('create-subject')
                            <a href="/admin/subject/create"><button class="btn btn-success pull-right">
                                    <i class="fa fa-plus">Add Subject</i></button></a>
                        @endpermission

                    </div>
                </div>
            </div>
            <div class="box-body table-responsive">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Subject</th>
                            <th>Total Question (1marks each)</th>
                            @permission('edit-subject')
                            <th>Edit</th>
                            @endpermission

                            @permission('delete-subject')
                            <th>Delete</th>
                            @endpermission
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 1)

                            @foreach($subject as $sub )

                                <tr>
                                    <th scope="row">{{$i}}</th>
                                    <td>{{$sub->subname}}</td>
                                    <td>{{$sub->weigh}}</td>

                                    @permission('edit-subject')
                                    <td style="text-align: center;">
                                        <div class="btn-group">
                                            <a href="{{'/admin/subject/'.$sub->id.'/edit'}}"><button type="button" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        </div>
                                    </td>
                                    @endpermission

                                    @permission('delete-subject')
                                    <td style="text-align: center;">
                                        <div class="btn-group">
                                            <form method="POST" action="{{'/admin/subject/'.$sub->id}}">
                                                {{csrf_field()}}
                                                {{method_field("DELETE")}}
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                            </form>

                                        </div>

                                    </td>
                                    @endpermission

                                </tr>

                                @php($i++)


                            @endforeach



                        </tbody>
                    </table>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
    @endpermission
</div>
<!-- /.content-wrapper -->






<!-- footer section -->

@include("admin.footer")