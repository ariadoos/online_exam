@include('admin.header')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Publish Result
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/Publish"><i class="fa fa-dashboard"></i>Publish Result</a></li>

        </ol>
    </section>
@include('errors.error')
<!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-8">
                        @foreach($data as $dat)
                        <h3 class="box-title"><h3>@if($dat->value == 0 )  Result is not publish to user
                            @elseif($dat->value == 1) Result is already published to user @endif</h3>
                    </div>
                    {{--<div class="col-xs-2"></div>--}}
                    {{--<div class="col-xs-2"></div>--}}
                    {{--<div class="col-xs-2"></div>--}}
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">

                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="/admin/publish">
                        {{csrf_field()}}
                        <div class="form-group row">

                            <label class="col-sm-3 form-control-label">Result</label>
                            <div class="col-sm-9 select">
                                <select  required name="decision" class="form-control" style="cursor:pointer" >


                                    <option value="0" @if($dat->value == 0 ) selected @endif>Not Published yet</option>
                                    <option value="1" @if($dat->value == 1 ) selected @endif>Publish the Result</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>



                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->





@include('admin.footer')