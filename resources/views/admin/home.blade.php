<!-- header section -->

@include("admin.header")


<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Result</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-2">
                        <span><i class="fa fa-graduation-cap"></i></span>
                        <h2 class="box-title">Result</h2>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-2">
                        @role('admin')
                        <a href="/admin/print" target="_blank"><button class="btn btn-success pull-right"><i class="fa fa-plus">Print Result</i></button></a>
                        @endrole
                          </div>
                </div>
            </div>
            <div class="box-body">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Student First Name</th>
                            <th>Last Name</th>
                            <th>Total Questions</th>
                            <th>Attempted Questions</th>
                            <th>Obtained Marks</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $res)
                        <tr>
                            @php($user = App\User::find($res->user_id))
                            @php($totalQuestion = App\answeruser::where('user_id' , '=' , $user->id)->count())
                            @php($unsolved =App\answeruser::where('answer_id' , '=' , NULL)->where('user_id' , '=' , $user->id)->count())
                            <td>{{ucfirst($user->firstname)}}</td>
                            <td>{{ucfirst($user->lastname)}}</td>
                            <td>
                                {{$totalQuestion}}
                            </td>
                            <td>{{$totalQuestion-$unsolved}}</td>
                            <td>{{$res->scores}}</td>
                        </tr>

                        @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body small-->

                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
            {{--box body big--}}
        </div>
        <!-- /.default box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- footer section -->

@include("admin.footer")
