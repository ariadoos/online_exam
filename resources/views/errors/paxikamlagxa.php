if ($this->attemptLogin($request)) {
if(($request->user()->role)!='admin')
{
$time = Settime::all();
$date = Carbon::now()->toDateString();
$current_time = Carbon::now()->setTimezone('Asia/Kathmandu')->toTimeString();
foreach($time as $time)
{
if(($date == $time->examdate) && (($current_time>=$time->startingtime)&&($current_time>=$time->endingtime)))
{
return $this->sendLoginResponse($request);
}
}
$this->guard()->logout();

$request->session()->invalidate();

return redirect('/')->withErrors(['The Exam is not in progress.']);;
}
else{
return $this->sendLoginResponse($request);
}