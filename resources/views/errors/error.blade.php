@if(count($errors)>0)

    <div class="alert alert-dismissible alert-danger">

        <button type="button" class="close" data-dismiss="alert">&times;</button>

        @foreach($errors->all() as $error)

            <strong> {{$error}}</strong>
            <br>

        @endforeach

    </div>

@endif


{{--alert of success  --}}
@if(Session::has('success'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{Session::get('success')}}</strong>
    </div>
@endif

{{--alert for failure --}}
@if(Session::has('error'))
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong> {{Session::get('error')}}</strong>
    </div>
@endif